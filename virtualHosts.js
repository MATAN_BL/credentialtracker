var express = require('express');
var vhost = require('vhost');

var bankServer = require("./bank/bankServer");
var loginServer = require("./tracking/loginServer");

var appWithVhost = express();
appWithVhost.use(vhost('bank-example.com', bankServer ));
appWithVhost.use(vhost('login-tracking.com', loginServer));

appWithVhost.listen(3000);
