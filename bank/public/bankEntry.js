var button = document.getElementsByTagName("button")[0];
button.addEventListener("click", clickButton, false);


function clickButton() {

   var username = document.getElementById('username').value;
   var password = document.getElementById('password').value;
   var ipaddr = "http://bank-example.com:3000";

   var xhttp = new XMLHttpRequest();
   var params = "username=" + username + "&password=" + password;

   xhttp.open("GET", ipaddr + "/login?" + params, true);
   xhttp.onreadystatechange = function () {
      console.log(xhttp.readyState + " " + xhttp.status);
      if (xhttp.readyState == 4 && xhttp.status == 200 ) {
         console.log("request " + params + " was sent to DB");
         window.location = "http://bank-example.com:3000/login?" + params;
      }
      if (xhttp.readyState == 4 && xhttp.status == 400) {
         console.error(xhttp.responseText);
         alert("Error! Please try later");
      }
      if (xhttp.readyState == 4 && xhttp.status == 401) {
         console.error(xhttp.responseText);
         alert("Authentication Failed");
      }
   };
   xhttp.send();

}